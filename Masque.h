#ifndef __MASQUE_H__
#define __MASQUE_H__


#include "CImg.h"
#include <vector>
#include <map>


using namespace cimg_library;
using namespace std;











/*
Class Masque contains : - mask : the binary image with white pixels (=255) for the pixels to inpaint .. 
                        - xOrigin, yOrigin : The center coordinates of the ellipse.
                        - xRadius, yRadius : The parameters of the ellipse (shape of the mask)
                        - PatchSize : the size of the patch
                        
*/

class Masque
{
public:
    CImg<unsigned int> mask;
    int xOrigin, yOrigin;
    int xRadius, yRadius;
    Masque(int patchSize, int width, int height, int x, int y, int xRad, int yRad);
    int PatchSize;
};















#endif
