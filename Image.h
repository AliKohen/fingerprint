#ifndef __IMAGE_H__
#define __IMAGE_H__
#include "CImg.h"
#include "Coord.h"
#include <string>
#include <vector>
#include <map>
#include "Masque.h"
#include "Patch.h"
using namespace std;
using namespace cimg_library;


class Image
{
        private:
                cimg_library::CImg<unsigned int> Im;
                unsigned int width;
                unsigned int height;
        public:
  		
  		// Constructor to initilize data
                //Image(cimg_library::CImg<unsigned int> image);
                Image(const char *const filename);
                
                // Convert pixels of type char to float
                cimg_library::CImg<float> casting_int_float();

                // Convert pixels of type float to char
                void casting_float_int(const cimg_library::CImg<float>& image);
  		
                // Methods to find the maximum and mininmum pixel intensity on the image 
                unsigned int maxIntensity();
                unsigned int minIntensity();
		
		// Getters
		unsigned int get_width();
		unsigned int get_height();

		
                // returns the symmetry of the image along the y-axis and saves it under "filename"
                cimg_library::CImg<unsigned int> ySymmetry(std::string filename);
                
                
                // returns the symmetry of the image along the y-axis without saving it
                cimg_library::CImg<unsigned int> ySymmetry();

                
		// Displays the image
		void Display(std::string namefile);
		
		// Draws rectangles on the image
		void Rectangle(unsigned int x, unsigned int y, unsigned int h, unsigned int w, unsigned int color);
		
		// Rotate the pixel coordinates
		Coord** motion_model(float teta);
		
		// Method to visualize the rotated image
		Coord** Visualize(float theta, float scalar);

		// Overloading () operator that returns the pixel values of the Image
		cimg_library::CImg<unsigned int> operator()();

		// operates the inverse rotation on the input image
		//void Inverse_warp(float theta, float scalar);
   		
		// Displays the output image after applying the first isotropic function
		void Iso_func1(unsigned int x, unsigned int y, unsigned int alpha);

                // Displays the output image after applying the first isotropic function                 
		void Iso_func2(unsigned int x, unsigned int y, float alpha);
		
                // Displays the output image after applying the first isotropic function
		void Iso_func3(unsigned int x, unsigned int y, float alpha);
		
                // Displays the output image after applying the first isotropic function
		void Aniso_func(unsigned int x, unsigned int y, float alpha, float a, float b);
		
		
		
		// Display the image Im
		void display();
                
              
                // Determines the center of the image under the maximum of blackness criteria
                pair<int, int> center();
                
                
                // Outputs the convenient mask for the image to restore
                Masque mask(int PatchSize);
                
                // Constructs a dictionary with 4 levels of complexity 1 - 2 - 3 - 4
                map<int, Patch> dictionary(Masque M, int level);
                
                // Erases all the pixels that must be inpainted again
                void clean(Masque &M);
                
                // Proceeds with the restoration of the image
                void restore(map<int, Patch> &dict, Masque &M, int PatchSize);
                
                // Principal function of the restoration, a smaller version of the function restore that provides intermediate results
                void dilate(map<int, Patch> &dict, Masque &M, int PatchSize, int hor, int ver);
};

// Rotates and translates the input image
cimg_library::CImg<unsigned int> Image_Warp(Coord **old_coord, cimg_library::CImg<unsigned int>old_values, Coord **new_coord);

// operates the inverse rotation on the input image
void Inverse_warp(cimg_library::CImg<unsigned int> rotated_Im, float theta, float scalar);
#endif
