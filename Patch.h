#ifndef __PATCH_H__
#define __PATCH_H__
#include "Masque.h"
#include <vector>
#include <map>

#include "CImg.h"


using namespace cimg_library;
using namespace std;






class Patch
{
private:
    vector<unsigned int> patch;
    unsigned int x, y;
    int size;
public:
    // Constructor
    Patch(int size, CImg<unsigned int> img, unsigned int xAxis, unsigned int yAxis);
    // Constructor with no entries to avoid errors
    Patch();
    // Compute the distance between two patches using only available pixels.
    int distance(Patch P, CImg<unsigned int> &mask);
    // Finds the closest Patch from the dictionary
    Patch closestPatch(map<int, Patch>, CImg<unsigned int> &mask);
    // Last step, inpaint the pixel with the middle pixel in the closest patch
    void fillWith(CImg<unsigned int> &img, CImg<unsigned int> &mask);
};










#endif
