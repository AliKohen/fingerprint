#include <iostream>
#include "Image.h"
#include "Utils.h"
#include "Coord.h"

using namespace std;
using namespace cimg_library;
//using namespace eigen;

int main(int argc, char const *argv[]){

	if(argc == 2){
		Image img = Image(argv[1]);
		cout << "Displaying different isotropic methods." << endl;
		cout << "The image corresponding to the first isotropic coefficient function is: "<<endl;
		img.Iso_func1((int)(122),(int)(232), 5);
		cout << "The image corresponding to the second  isotropic coefficient function is: "<<endl;
		img.Iso_func2((int)(122),(int)(232), 10);
		cout << "The image corresponding to the isotropic gaussian coefficient function is: " << endl;
		img.Iso_func3((int)(122),(int)(232), 0.001);
		cout << "The image corresponding to the antisotropic gaussian coefficient function is: " << endl;
                img.Aniso_func((int)(122),(int)(232), 0.02, 0.1, 0.8);
    	}
	return 0;
}
