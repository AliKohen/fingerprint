#include "Utils.h"

using namespace cimg_library;
using namespace std;
using namespace Eigen;

/**
  * Method to compute the distance between two points
  *
  * @param x1 x-coord of the first point
  * @param y1 y-coord of the first point
  * @param x2 x-coord of the second point
  * @param y2 y-coord of the second point
  */

float dist(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2){
  float dist;
  dist = pow((x1 - x2), 2) + pow((y1 - y2), 2);
  dist = sqrt(dist);
  return dist;
}

/**
  * Method to compute the distance between two points
  *
  * @param x1 x-coord of the first point
  * @param y1 y-coord of the first point
  * @param x2 x-coord of the second point
  * @param y2 y-coord of the second point
  * @param a the minor axisof the ellipse shape
  * @param b the major axis of the ellipse shape
  */

float aniso_dist(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, float a, float b)
{
	float dist;
	dist = pow(((x1 - x2)/a), 2) + pow(((y1 - y2)/b), 2);
	dist = sqrt(dist);
	return dist;
}
