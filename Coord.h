#ifndef __COORD_H__
#define __COORD_H__

class Coord{

        friend std::ostream& operator<<(std::ostream& os, const Coord& c);
        private:
          float x,y;
        public:
          // Constructors
          Coord();
          Coord(unsigned int x, unsigned int y);
          Coord(float x, float y);
          // Rotate the coordinates
		      void rotation(float theta, unsigned int x0, unsigned int y0);  
	// Translate the coordinates
		      void translation(float scalar, unsigned int x0, unsigned int y0);
			void translation(float scalar);
          // Warp the coordinates ( rotation + translation)
		      void warp(float theta, float scalar, unsigned int x0, unsigned int y0);
		      // Setters
          void Set_x(float t);
		      void Set_y(float t);
		      // Getters
		      float Get_x();
		      float Get_y();
		      // Overloading assign operator 
		      Coord& operator=(const Coord& c);
		
};

#endif

