#!/bin/bash

alias restoration='g++ Image.cpp restoration.cpp Patch.cpp Masque.cpp Utils.cpp Coord.cpp -o restoration -lm -lpthread -lX11'
alias simulation='g++ Image.cpp simulation.cpp Patch.cpp Masque.cpp Utils.cpp Coord.cpp -o simulation -lm -lpthread -lX11'
alias rotation='g++ Image.cpp rotation.cpp Patch.cpp Masque.cpp Utils.cpp Coord.cpp -o rotation -lm -lpthread -lX11'
alias morphofilters='g++ -o morphofilters.exe morphofilters.cpp -lm -lpthread -lX11'

