#include <iostream>
#include "Image.h"
#include <string>
#include <cassert>

using namespace cimg_library;
using namespace std;

int main(int argc, char const *argv[]) {
  if(argc == 2)
  {
    /*
    Input image
    */
    Image toRestore(argv[1]);
    
    /*
    Define the size of the patch
    */
    cout << "Enter the size of the patch as an odd number between: 7 - 9 - 11 - 13 - 15 - 17" << endl;
    unsigned int PatchSize;
    cin >> PatchSize;
    
    /* Define the MASK */
    Masque M = toRestore.mask(PatchSize);
    M.mask.display();
    
    
    cout << "Select a level of complexity for the dictionary : 'i' - 3000*i" << endl;
    cout << "Enter i between 1 - 2 - 3 - 4 :" << endl;
    int level;
    cin >> level;
    assert (level == 1 or level == 2 or level == 3 or level == 4);
    map<int, Patch> dict = toRestore.dictionary(M, level);
    
    
    /*
    Clean the image - It removes the part to inpaint again
    */
    toRestore.clean(M);
    toRestore.display();
    
    
    /*
    Proceeds with the restoration
    */
    toRestore.restore(dict, M, PatchSize);
    
    toRestore.display();
    
    
  }
  return 0;
}
